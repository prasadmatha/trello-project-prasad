import axios from "axios";

let key = "454aef0a8574cb896b9e3066f8f3e877";
let token = "2f91ff5c6c582a9ebe024a37277a1441bd2909fe36d3d6e2bff5a8544bf4d0dc";

let authenticationDetails = { key: key, token: token };

localStorage.setItem(
  "authenticationDetails",
  JSON.stringify(authenticationDetails)
);

export async function getBoardsData() {
  return await axios
    .get(`https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`)
    .then((res) => res.data)
    .catch((error) => error);
}

export async function createBoard(name) {
  return await axios
    .post(
      `https://api.trello.com/1/boards?name=${name}&key=${key}&token=${token}`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function getListsData(id) {
  return await axios
    .get(
      `https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function createList(id, name) {
  return await axios
    .post(
      `https://api.trello.com/1/boards/${id}/lists?name=${name}&pos=bottom&key=${key}&token=${token}`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function deleteList(id) {
  return await axios
    .put(
      `https://api.trello.com/1/lists/${id}/closed?key=${key}&token=${token}&value=true`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function createCard(listId, name, desc) {
  return await axios
    .post(
      `https://api.trello.com/1/cards?name=${name}&desc=${desc}&pos=bottom&idList=${listId}&key=${key}&token=${token}`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function getAllCards(listId) {
  return await axios
    .get(
      `https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function deleteCard(cardId) {
  return await axios
    .delete(
      `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function createChecklist(cardId, name) {
  return await axios
    .post(
      `https://api.trello.com/1/checklists?idCard=${cardId}&key=${key}&token=${token}&name=${name}&pos=bottom`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function getAllChecklists(cardId) {
  return axios
    .get(
      `https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function getCheckItems(id) {
  return axios
    .get(
      `https://api.trello.com/1/checklists/${id}/checkItems?key=${key}&token=${token}`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function createCheckItem(checklistId, checkItemName) {
  return axios
    .post(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${checkItemName}&key=${key}&token=${token}&pos=bottom`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

export async function deleteChecklist(checklistId) {
  return axios
    .delete(
      `https://api.trello.com/1/checklists/${checklistId}?key=${key}&token=${token}`
    )
    .then((res) => console.log(res.data))
    .catch((error) => console.log(error));
}

export async function deleteCheckItem(checklistId, checkItemId) {
  return axios
    .delete(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${key}&token=${token}`
    )
    .then((res) => console.log(res.data))
    .catch((error) => console.log(error));
}

export async function updateCheckItem(cardId, checklistId, checkItemId, state) {
  return axios
    .put(
      `https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}?key=${key}&token=${token}&state=${state}`
    )
    .then((res) => res.data)
    .catch((error) => console.log(error));
}

import React, { Component } from "react";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import * as api from "../../api";
import "./index.css";

class CardComponent extends Component {
  state = {};

  render() {
    return (
      <div
        style={{
          width: "100%",
          padding: "0.2rem",
          border: "solid 1px white",
          marginBottom: "0.5rem",
        }}
        className="d-flex"
      >
        <Link
          to={`/cards/${this.props.name}/${this.props.cardId}/checklists/`}
          style={{ textDecoration: "none" }}
        >
          <Card className="card" style={{ textDecoration: "none" }}>
            <Card.Body>{this.props.name}</Card.Body>
          </Card>
        </Link>

        <button
          style={{ height: "1.8rem", width: "1.9rem" }}
          onClick={() => this.props.delete(this.props.cardId)}
        >
          <i className="fa-solid fa-box-archive" style={{ marginTop: "0" }}></i>
        </button>
      </div>
    );
  }
}

export default CardComponent;

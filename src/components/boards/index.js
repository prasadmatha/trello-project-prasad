import React, { Component } from "react";
import * as api from "../../api";
import Board from "./board";
import Loader from "react-loader-spinner";
import ModalComponent from "./modal";
import "./index.css";

class Boards extends Component {
  state = { boardsData: undefined, isLoading: true };
  componentDidMount() {
    this.boardsData();
  }

  boardsData = async () => {
    await api
      .getBoardsData()
      .then((res) => this.setState({ boardsData: res, isLoading: false }));
  };

  handleCreateBoard = async (boardName) => {
    let newBoardData = await api.createBoard(boardName).then((res) => res);
    this.setState({ boardsData: [...this.state.boardsData, newBoardData] });
  };

  render() {
    if (this.state.isLoading) {
      return (
        <Loader
          type="TailSpin"
          color="red"
          height={50}
          width={50}
          style={{ position: "absolute", top: "50%", left: "50%" }}
        />
      );
    } else {
      return (
        <>
          <h3
            style={{
              textDecoration: "underline",
              textAlign: "center",
              color: "navy",
            }}
          >
            BOARDS
          </h3>
          <div className="d-flex flex-wrap justify-content-between">
            <div
              className="board d-flex justify-content-center"
              style={{ alignItems: "center", borderRadius: "0.5rem" }}
            >
              <ModalComponent handleCreateBoard={this.handleCreateBoard} />
            </div>
            {this.state.boardsData !== undefined &&
              this.state.boardsData
                .reverse(true)
                .map((each) => (
                  <Board
                    board={each}
                    key={each.id}
                    name={each.name}
                    boardId={each.id}
                  />
                ))}
          </div>
        </>
      );
    }
  }
}

export default Boards;

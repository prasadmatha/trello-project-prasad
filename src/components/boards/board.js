import React, { Component } from "react";
import { Link } from "react-router-dom";

class Board extends Component {
  state = {};

  render() {
    let id = this.props.boardId;

    return (
      <Link to={`/boards/${id}/lists`} style={{ textDecoration: "none" }}>
        <div className="board">
          <span
            style={{
              textDecoration: "none",
              backgroundColor: "yellow",
              fontSize: "1.2rem",
              fontWeight: "500",
              padding: "0.3rem",
              border: "solid 2px navy",
            }}
          >
            {this.props.name}
          </span>
        </div>
      </Link>
    );
  }
}

export default Board;

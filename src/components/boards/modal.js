import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";

class ModalComponent extends Component {
  state = { show: false, setShow: false };

  handleSubmit = (event) => {
    event.preventDefault();
    let boardName = event.target[0].value;
    if (boardName !== "") {
      this.props.handleCreateBoard(boardName);
      this.setState({ show: !this.state.show });
    }
  };

  handleShow = () => this.setState({ show: !this.state.show });

  render() {
    return (
      <>
        <Button
          variant="info"
          style={{ border: "1px solid white" }}
          onClick={() => this.handleShow()}
        >
          Create a new board
        </Button>

        <Modal
          show={this.state.show}
          onHide={this.handleShow}
          size="md"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>User Input Dialogue Box</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>User Input</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Please give your input here"
                  autoFocus
                />
              </Form.Group>
              <Form.Group>
                <Button
                  variant="primary"
                  type="submit"
                  onSubmit={this.handleSubmit}
                >
                  Save
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.handleShow}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default ModalComponent;

import React, { Component } from "react";
import * as api from "../../api";
import List from "./list";
import ModalComponent from "./modalForList";
import Loader from "react-loader-spinner";
import "./index.css";

class Lists extends Component {
  state = { listsData: undefined, boardId: "", isLoading: true };

  componentDidMount() {
    let boardId = this.props.match.params.id;
    this.setState({ boardId });
    api
      .getListsData(boardId)
      .then((res) => this.setState({ listsData: res, isLoading: false }));
  }

  handleCreateList = async (listName) => {
    let newListData = await api
      .createList(this.state.boardId, listName)
      .then((res) => res);
    this.setState({ listsData: [...this.state.listsData, newListData] });
  };

  handleDeleteList = async (listId) => {
    await api.deleteList(listId);
    let listsData = this.state.listsData.filter((each) => each.id !== listId);
    this.setState({ listsData });
  };

  render() {
    if (this.state.isLoading) {
      return (
        <Loader
          type="TailSpin"
          color="red"
          height={50}
          width={50}
          style={{ position: "absolute", top: "50%", left: "50%" }}
        />
      );
    } else {
      return (
        <>
          <h3
            style={{
              textDecoration: "underline",
              textAlign: "center",
              color: "navy",
            }}
          >
            LISTS & CARDS
          </h3>
          <div
            className="d-flex "
            style={{
              minHeight: "100vh",
              overflow: "auto",
              alignItems: "flex-start",
            }}
          >
            <div
              className="list d-flex justify-content-center"
              style={{
                color: "black",
                fontWeight: "500",
                fontSize: "1.2rem",
                alignItems: "center",
                height: "5rem",
              }}
            >
              <ModalComponent handleCreateList={this.handleCreateList} />
            </div>

            {this.state.listsData.map((each) => (
              <List
                list={each}
                key={each.id}
                name={each.name}
                listId={each.id}
                boardId={this.state.boardId}
                delete={this.handleDeleteList}
              />
            ))}
          </div>
        </>
      );
    }
  }
}

export default Lists;

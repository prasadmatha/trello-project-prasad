import React, { Component } from "react";
import Card from "../cards";
import Loader from "react-loader-spinner";
import * as api from "../../api";
import ModalComponentForCard from "./modalForCard";
import "./index.css";

class List extends Component {
  state = { cardsData: undefined };

  componentDidMount() {
    api
      .getAllCards(this.props.listId)
      .then((res) => this.setState({ cardsData: res }));
  }

  handleCreateCard = async (cardName) => {
    let newCardData = await api
      .createCard(this.props.listId, cardName)
      .then((res) => res);
    this.setState({ cardsData: [...this.state.cardsData, newCardData] });
  };

  handleDeleteCard = async (cardId) => {
    await api.deleteCard(cardId);
    let cardsData = this.state.cardsData.filter((each) => each.id !== cardId);
    this.setState({ cardsData });
  };

  render() {
    return (
      <div className="list">
        <div
          className="d-flex justify-content-between"
          style={{
            color: "black",
            fontSize: "1.3rem",
            fontWeight: "500",
            height: "4rem",
          }}
          onClick={() => this.props.delete(this.props.listId)}
        >
          {this.props.name}
          <button style={{ height: "2rem" }}>
            <i
              className="fa-solid fa-box-archive"
              style={{ marginTop: "0.2rem" }}
            ></i>
          </button>
        </div>
        <ModalComponentForCard handleCreateCard={this.handleCreateCard} />
        {this.state.cardsData !== undefined &&
          this.state.cardsData.map((each) => (
            <Card
              card={each}
              cardId={each.id}
              key={each.id}
              name={each.name}
              desc={each.desc}
              delete={this.handleDeleteCard}
            />
          ))}
      </div>
    );
  }
}

export default List;

import React, { Component } from "react";
import Progress from "./progressBar";
import * as api from "../../api";
import CheckItem from "../checkitems";
import Loader from "react-loader-spinner";
import ModalComponentForCheckItem from "./modalCheckItem";

class Checklist extends Component {
  state = {
    checkItemsData: undefined,
    isLoading: true,
    noOfCheckedItems: 0,
    progressBarValue: 0,
  };

  getAllCheckItems = async () => {
    let checkItemsData = await api
      .getCheckItems(this.props.id)
      .then((res) => res);

    let noOfCheckedItems = checkItemsData.filter(
      (each) => each.state === "complete"
    );

    noOfCheckedItems = noOfCheckedItems.length;

    this.setState({
      checkItemsData: checkItemsData,
      noOfCheckedItems: noOfCheckedItems,
    });
  };

  componentDidMount() {
    this.getAllCheckItems();
  }

  handleDelete = async (checkItemId) => {
    await api.deleteCheckItem(this.props.id, checkItemId);
    let checkItemsData = this.state.checkItemsData.filter(
      (each) => each.id !== checkItemId
    );
    let noOfCheckedItems = checkItemsData.filter(
      (each) => each.state === "complete"
    ).length;
    this.setState({
      checkItemsData: checkItemsData,
      noOfCheckedItems: noOfCheckedItems,
    });
  };

  handleCreateCheckItem = async (checkItemName) => {
    let checklistId = this.props.id;

    let newCheckItemData = await api
      .createCheckItem(checklistId, checkItemName)
      .then((res) => res);

    this.setState({
      checkItemsData: [...this.state.checkItemsData, newCheckItemData],
    });
  };

  handleUpdateCheckItems = async (checkItemId, state) => {
    let cardId = this.props.checklist.idCard;
    let checklistId = this.props.checklist.id;

    let updatedDataObject = await api
      .updateCheckItem(cardId, checklistId, checkItemId, state)
      .then((res) => res);

    let checkItemsData = this.state.checkItemsData.map((each) => {
      return each.id === updatedDataObject.id ? updatedDataObject : each;
    });

    let noOfCheckedItems = checkItemsData.filter(
      (each) => each.state === "complete"
    );
    noOfCheckedItems = noOfCheckedItems.length;

    this.setState({
      checkItemsData: checkItemsData,
      noOfCheckedItems: noOfCheckedItems,
    });
  };

  render() {
    if (this.state.checkItemsData !== undefined) {
      return (
        <div
          className="d-flex flex-column"
          style={{
            width: "100%",
            marginBottom: "3rem",
            padding: "0.5rem",
            border: "solid 1px black",
          }}
        >
          <div
            className="d-flex justify-content-between "
            style={{ marginBottom: "1rem", alignItems: "center" }}
          >
            <div>
              <i className="fa-solid fa-square-check"></i>
              <label htmlFor={this.props.id}> {this.props.name}</label>
            </div>
            <button
              className="btn btn-danger"
              onClick={() => this.props.handleDelete(this.props.id)}
            >
              Delete
            </button>
          </div>
          <Progress
            progressBarValue={
              (100 / this.state.checkItemsData.length) *
              this.state.noOfCheckedItems
            }
          />
          {this.state.checkItemsData.map((each) => {
            return (
              <CheckItem
                each={each}
                id={each.id}
                name={each.name}
                key={each.id}
                handleDelete={this.handleDelete}
                state={each.state}
                handleUpdateCheckItems={this.handleUpdateCheckItems}
                handleProgressBar={this.handleProgressBar}
              />
            );
          })}
          <ModalComponentForCheckItem
            handleCreateCheckItem={this.handleCreateCheckItem}
          />
        </div>
      );
    }
  }
}

export default Checklist;

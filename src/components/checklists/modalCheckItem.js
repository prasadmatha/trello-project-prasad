import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";

class ModalComponentForCheckItem extends Component {
  state = { show: false, setShow: false };

  handleSubmit = (event) => {
    event.preventDefault();
    let checkItemName = event.target[0].value;
    if (checkItemName !== "") {
      this.props.handleCreateCheckItem(checkItemName);
      this.setState({ show: !this.state.show });
    }
  };

  handleShow = () => this.setState({ show: !this.state.show });

  render() {
    return (
      <>
        <Button
          variant="info"
          style={{
            height: "2rem",
            width: "6rem",
            fontSize: "0.8rem",
            border: "solid 1px black",
          }}
          onClick={() => this.handleShow()}
        >
          Add an Item
        </Button>

        <Modal
          show={this.state.show}
          onHide={this.handleShow}
          size="md"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>User Input Dialogue Box</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>User Input</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Please give your input here"
                  autoFocus
                />
              </Form.Group>
              <Form.Group>
                <Button
                  variant="primary"
                  type="submit"
                  onSubmit={this.handleSubmit}
                >
                  Save
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.handleShow}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default ModalComponentForCheckItem;

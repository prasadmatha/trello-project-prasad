import React, { Component } from "react";
import * as api from "../../api";
import Checklist from "./checklist";
import ModalComponent from "./modal";
import Loader from "react-loader-spinner";

class Checklists extends Component {
  state = { checklistsData: undefined, name: "", isLoading: true };

  getChecklistsData = async (cardId) => {
    await api
      .getAllChecklists(cardId)
      .then((res) => this.setState({ checklistsData: res }));
    this.setState({ name: this.props.match.params.name, isLoading: false });
  };

  handleCreateChecklist = async (checklistName) => {
    let cardId = this.props.match.params.cardId;
    let newChecklistData = await api
      .createChecklist(cardId, checklistName)
      .then((res) => res);
    this.setState({
      checklistsData: [...this.state.checklistsData, newChecklistData],
    });
  };

  componentDidMount() {
    const { cardId } = this.props.match.params;
    this.getChecklistsData(cardId);
  }

  handleDelete = async (checklistId) => {
    await api.deleteChecklist(checklistId);
    let checklistsData = this.state.checklistsData.filter(
      (each) => each.id !== checklistId
    );
    this.setState({ checklistsData });
  };

  render() {
    let checklistsData = this.state.checklistsData;
    if (this.state.isLoading) {
      return (
        <Loader
          type="TailSpin"
          color="red"
          height={50}
          width={50}
          style={{ position: "absolute", top: "50%", left: "50%" }}
        />
      );
    } else {
      return (
        <div
          style={{
            width: "100vw",
            padding: "2rem",
            alignItems: "center",
            minHeight: "100vh",
          }}
          className="d-flex flex-column"
        >
          <div
            className="d-flex justify-content-between"
            style={{ width: "80%", marginBottom: "4rem" }}
          >
            <h3>{this.state.name}</h3>
            <ModalComponent
              handleCreateChecklist={this.handleCreateChecklist}
            />
          </div>
          <div style={{ width: "80%" }}>
            {checklistsData.map((each) => {
              return (
                <Checklist
                  checklist={each}
                  key={each.id}
                  id={each.id}
                  name={each.name}
                  handleDelete={this.handleDelete}
                />
              );
            })}
          </div>
        </div>
      );
    }
  }
}

export default Checklists;

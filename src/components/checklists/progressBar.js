import React, { Component } from "react";
import ProgressBar from "react-bootstrap/ProgressBar";

class Progress extends Component {
  state = { progressValue: null };
  render() {
    return (
      <ProgressBar
        animated
        now={this.props.progressBarValue}
        style={{ marginBottom: "1rem" }}
        max={100}
      />
    );
  }
}

export default Progress;

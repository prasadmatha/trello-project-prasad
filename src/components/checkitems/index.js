import React, { Component } from "react";

class CheckItem extends Component {
  
  state = { state: this.props.state };

  handleUpdate = async (e) => {
    let state = e.target.checked ? "complete" : "incomplete";
    this.setState({ state });
    await this.props.handleUpdateCheckItems(this.props.id, state);
  };

  componentDidMount() {
    let state = this.props.state;
    console.log(this.props.state);
    this.setState({ state });
  }

  render() {
    return (
      <div
        className="d-flex justify-content-between"
        style={{ margin: "1rem 0", alignItems: "center" }}
      >
        <div>
          <input
            type="checkbox"
            id={this.props.id}
            style={{ marginRight: "0.2rem" }}
            defaultChecked={this.state.state === "complete" ? true : false}
            onChange={this.handleUpdate}
          ></input>

          <label
            htmlFor={this.props.id}
            className={
              this.state.state === "complete"
                ? "text-decoration-line-through"
                : "text-decoration-none"
            }
          >
            {this.props.name}
          </label>
        </div>

        <button
          className="btn btn-danger"
          onClick={() => this.props.handleDelete(this.props.id)}
        >
          Delete
        </button>
      </div>
    );
  }
}

export default CheckItem;

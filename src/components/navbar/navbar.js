import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Offcanvas from "react-bootstrap/Offcanvas";
import logo from "../../../src/logo.jpg";

function NavbarComponent() {
  return (
    <>
      {["lg"].map((expand) => (
        <Navbar key={expand} bg="primary" className="mb-3">
          <Container fluid>
            <Navbar.Brand href="#">
              <div></div>
              <img
                src={logo}
                alt="logo"
                style={{
                  height: "3rem",
                  width: "8rem",
                  backgroundColor: "white",
                }}
              />
            </Navbar.Brand>

            <Nav className="justify-content-start flex-grow-1 pe-3">
              <Nav.Link href="/" className="text-light">
                <button
                  className="btn btn-warning text-dark"
                  style={{
                    height: "3rem",
                    width: "5rem",
                    fontWeight: "bold",
                  }}
                >
                  HOME
                </button>
              </Nav.Link>
            </Nav>
            <h1 className="text-warning mt-1">PRASAD MATHA</h1>
          </Container>
        </Navbar>
      ))}
    </>
  );
}

export default NavbarComponent;

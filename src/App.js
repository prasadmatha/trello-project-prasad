import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import NavbarComponent from "./components/navbar/navbar.js";
import Boards from "./components/boards/index.js";
import Lists from "./components/lists/index.js";
import Checklists from "./components/checklists/index.js";
import "./App.css";

class App extends Component {
  state = {};
  render() {
    return (
      <Router>
        <NavbarComponent />
        <Switch>
          <Route exact path="/">
            <Boards />
          </Route>
          <Route exact path="/boards/">
            <Boards />
          </Route>
          <Route exact path="/boards/:id/lists/" component={Lists} />
          <Route
            exact
            path="/cards/:name/:cardId/checklists/"
            component={Checklists}
          />
        </Switch>
      </Router>
    );
  }
}

export default App;
